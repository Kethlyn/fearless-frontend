function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
      <div class="card mb-3 shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title"> ${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">

          ${new Date(starts).toLocaleDateString()} -
          ${new Date(ends).toLocaleDateString()}
        </div>
      </div>

    `;
  }
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new error(" The response is not ok") // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let columns = document.getElementsByClassName('col');
        let currentColumn = 0;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log("details", details);
                const location = details.conference.location.name;
                console.log("location", location);
                // const starts = new Date(details.conference.starts).toLocaleDateString();
                const starts = details.conference.starts;
                console.log("this is the type of data for starts", starts);
                const ends = details.conference.ends;
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(name, description, pictureUrl, location, starts, ends);
                // const column = document.querySelector('.col');
                const column = columns[currentColumn];
                column.innerHTML += html;
                console.log(details);
                if (currentColumn === 2){
                    currentColumn =0;

                }else{
                    currentColumn ++;
                }

            }
        }
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //   const details = await detailResponse.json();
        //   const descriptionTag = document.querySelector('.card-text');
        //   descriptionTag.innerHTML = details.conference.description;
        //   console.log(details);

        // const  imageTag= document.querySelector('.card-img-top');
        // imageTag.src = details.conference.location.picture_url;

        // }

      }
    } catch (e) {
        console.error("Clearly, something's wrong. You may want to troubleshoot:", e); // Figure out what to do if an error is raised
    }

  });